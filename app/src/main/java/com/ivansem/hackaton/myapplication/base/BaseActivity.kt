package ru.terrakok.gitlabclient.ui.global

import android.support.v7.app.AppCompatActivity
import com.ivansem.hackaton.myapplication.base.BasePresenter

/**
 * @author Konstantin Tskhovrebov (aka terrakok) on 26.03.17.
 */
abstract class BaseActivity : AppCompatActivity() {

    protected abstract val presenter:BasePresenter

    override fun onStop() {
        super.onStop()
        presenter.onDestroy()
    }
}