package com.ivansem.hackaton.myapplication.main

import android.util.Log
import com.ivansem.hackaton.myapplication.base.BasePresenter
import com.ivansem.hackaton.myapplication.db.DbService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by root on 05.04.18.
 */
class MainPresenter(private val view: IMainView): BasePresenter() {
    fun loadMembers() {
        //асинхронная загрузка из БД с помощью RxJava
        DbService.getInstance().getMembers()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.showMembers(it.map { Member.toStandartForm(it) })
                },{error -> Log.e(TAG, error.message, error)}).connect()
    }

    companion object {
        val TAG = "#MY " + MainPresenter::class.java.simpleName
    }
}