package com.ivansem.hackaton.myapplication.details

import android.util.Log
import com.ivansem.hackaton.myapplication.base.BasePresenter
import com.ivansem.hackaton.myapplication.db.DbService
import com.ivansem.hackaton.myapplication.main.MainPresenter
import com.ivansem.hackaton.myapplication.main.Member
import com.ivansem.hackaton.myapplication.members.IMemberCreateView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by root on 06.04.18.
 */
class MemberDetailsPresenter(private val view: IMemberDetailsView) : BasePresenter(){

    fun loadMember(id : Long) {
        DbService.getInstance().getMember(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.showMember( Member.toStandartForm(it))
                },{error -> Log.e(TAG, error.message, error)})
                .connect()
    }

    companion object {
        val TAG = "#MY " + MainPresenter::class.java.simpleName
    }
}