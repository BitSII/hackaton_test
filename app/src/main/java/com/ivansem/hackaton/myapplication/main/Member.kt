package com.ivansem.hackaton.myapplication.main

import com.ivansem.hackaton.myapplication.db.DbMember

/**
 * Created by root on 05.04.18.
 */
data class Member(val name: String, val age: Int, val group: String, val info: String, val imgName: String, val id: Long? = null) {

    companion object {
        val TAG = "#MY " + Member::class.java.simpleName

        fun toDbForm(member: Member): DbMember {
            val dbMember = DbMember()
            dbMember.age = member.age
            dbMember.name = member.name
            dbMember.group = member.group
            dbMember.info = member.info
            dbMember.imgName = member.imgName
            return dbMember
        }
//
        fun toStandartForm(dbMember: DbMember): Member {
            return Member(
                    dbMember.name,
                    dbMember.age,
                    dbMember.group,
                    dbMember.info,
                    dbMember.imgName,
                    dbMember.id
            )
        }
    }
}