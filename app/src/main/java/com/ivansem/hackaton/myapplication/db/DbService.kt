package com.ivansem.hackaton.myapplication.db

import android.util.Log
import com.ivansem.hackaton.myapplication.main.Member
import io.reactivex.Single
import io.realm.Realm
import io.realm.RealmConfiguration

/**
 * Created by root on 05.04.18.
 */
class DbService {
    fun saveMember(member: Member): Single<Member> {
        return Single.just(member).map {
            val realm = Realm.getInstance(realmConfig)
            realm.beginTransaction()

            //автоинкремент ИД, реалм из коробки не поддерживает
            val id = realm.where(DbMember::class.java).max("id")?.toLong() ?: 0L
            val dbMember = Member.toDbForm(member)
            dbMember.id = id + 1
            realm.copyToRealm(dbMember)
            realm.commitTransaction()
            Log.d(TAG, "saved ${member.name}")
            member.copy(id = dbMember.id)
        }
    }

    fun getMembers(): Single<List<DbMember>> {
        return Single.just(Realm.getInstance(realmConfig).where(DbMember::class.java).findAll().toList())
    }
    fun getMember(id : Long): Single<DbMember> {
        return Single.just(Realm.getInstance(realmConfig).where(DbMember::class.java).equalTo("id",id).findFirst())
    }

    fun erase() {
        val realm = Realm.getInstance(realmConfig)
        realm.beginTransaction()
        realm.deleteAll()
        realm.commitTransaction()
    }

    fun getMemberCount(): Int {
        return Realm.getInstance(realmConfig).where(DbMember::class.java).count().toInt()
    }

    companion object {
        val TAG = "#MY " + DbService::class.java.simpleName
        val realmConfig = RealmConfiguration.Builder()
                .name("main.realm")
                .deleteRealmIfMigrationNeeded()
                .schemaVersion(3)
                .build()!!

        @JvmField
        protected val instance: DbService = DbService()

        fun getInstance(): DbService {
            return instance
        }
    }
}