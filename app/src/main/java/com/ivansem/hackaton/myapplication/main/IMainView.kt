package com.ivansem.hackaton.myapplication.main

/**
 * Created by root on 05.04.18.
 */
interface IMainView {
    fun showMembers(members: List<Member>)
}