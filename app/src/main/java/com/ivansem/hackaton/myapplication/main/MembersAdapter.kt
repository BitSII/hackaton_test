package com.ivansem.hackaton.myapplication.main

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ivansem.hackaton.myapplication.R
import com.ivansem.hackaton.myapplication.loadRoundedImage
import com.ivansem.hackaton.myapplication.utils.FileUtils
import kotlinx.android.synthetic.main.item_member.view.*

/**
 * Created by root on 05.04.18.
 */
class MembersAdapter : RecyclerView.Adapter<MembersAdapter.MemberViewHolder>() {
    private val members: ArrayList<Member> = ArrayList(8)
    public var clickListener: ((id: Long?) -> Unit)? = null


    fun swap(newList: List<Member>) {
        members.clear()
        members.addAll(newList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MemberViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_member, parent, false)
        return MemberViewHolder(view)
    }

    override fun getItemCount(): Int {
        return members.size
    }

    override fun onBindViewHolder(holder: MemberViewHolder, position: Int) {
        holder.bind(members[position])
    }

    inner class MemberViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val nameTv = itemView.item_member_name_tv
        private val groupTv = itemView.item_member_group_tv
        private val image = itemView.item_member_img

        fun bind(member: Member) {
            nameTv.text = member.name
            itemView.setOnClickListener({
                clickListener?.invoke(member.id)
            })
            groupTv.text = member.group
            val memberImg = FileUtils.getMemberImg(member.imgName)
            image.loadRoundedImage(memberImg.absolutePath, image.context)
        }
    }

    fun setOnClickListener(listener: (id:Long?) -> Unit) {
        this.clickListener = listener
    }


    companion object {
        val TAG = "#MY " + MembersAdapter::class.java.simpleName
    }

    interface OnClickListener {
        fun onClick(id: Long?)
    }
}