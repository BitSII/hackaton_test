package com.ivansem.hackaton.myapplication.members

/**
 * Created by root on 06.04.18.
 */
interface IMemberCreateView {
    fun memberCreated()
    fun memberNotCreated()
}