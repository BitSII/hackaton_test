package com.ivansem.hackaton.myapplication

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.BitmapImageViewTarget
import java.io.ByteArrayOutputStream


fun ImageView.loadRoundedImage(
        url: String?,
        ctx: Context? = null
) {
    Glide.with(ctx ?: context)
            .load(url)
            .asBitmap()
            .centerCrop()
            .into(object : BitmapImageViewTarget(this) {
                override fun onLoadStarted(placeholder: Drawable?) {
                    setImageResource(R.drawable.photo_placeholder)
                }

                override fun onLoadFailed(e: java.lang.Exception?, errorDrawable: Drawable?) {
                    setImageResource(R.drawable.photo_placeholder)
                }

                override fun setResource(resource: Bitmap?) {
                    resource?.let {
                        RoundedBitmapDrawableFactory.create(view.resources, it).run {
                            this.isCircular = true
                            setImageDrawable(this)
                        }
                    }
                }
            })
}

fun ImageView.loadRoundedImage(
        bitmap: Bitmap,
        ctx: Context? = null
) {
    val stream = ByteArrayOutputStream()
    bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)
    Glide.with(ctx ?: context)
            .load(stream.toByteArray())
            .asBitmap()
            .centerCrop()
            .into(object : BitmapImageViewTarget(this) {
                override fun onLoadStarted(placeholder: Drawable?) {
                    setImageResource(R.drawable.photo_placeholder)
                }

                override fun onLoadFailed(e: java.lang.Exception?, errorDrawable: Drawable?) {
                    setImageResource(R.drawable.photo_placeholder)
                }

                override fun setResource(resource: Bitmap?) {
                    resource?.let {
                        RoundedBitmapDrawableFactory.create(view.resources, it).run {
                            this.isCircular = true
                            setImageDrawable(this)
                        }
                    }
                }
            })
}

