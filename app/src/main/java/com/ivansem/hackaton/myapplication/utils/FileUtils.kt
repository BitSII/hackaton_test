package com.ivansem.hackaton.myapplication.utils

import android.graphics.Bitmap
import android.util.Log
import com.ivansem.hackaton.myapplication.MyApp
import java.io.File
import java.io.FileOutputStream
import java.util.*

/**
 * Created by root on 05.04.18.
 */
object FileUtils {
    fun saveMemberImg(bitmap: Bitmap): String {
        val name = "member" + Random().nextInt(1000000000) + ".png"
        val path = getDir().absolutePath + "/" + name
        Log.d("#MY ", "saving into $path")
        val file = File(path)
        file.createNewFile()


        val outStream = FileOutputStream(file)
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, outStream)
        outStream.flush()
        outStream.close()

        return name
    }

    fun getMemberImg(name: String): File {
        return File(getDir().absolutePath + "/" + name)
    }
    private fun getDir(): File {
        return MyApp.instance.filesDir
    }

}