package com.ivansem.hackaton.myapplication.main

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import com.ivansem.hackaton.myapplication.R
import com.ivansem.hackaton.myapplication.details.MemberDetailsActivity
import com.ivansem.hackaton.myapplication.members.MemberCreateActivity
import kotlinx.android.synthetic.main.activity_main.*
import ru.terrakok.gitlabclient.ui.global.BaseActivity

class MainActivity : BaseActivity(), IMainView {
    private var mAdapter: MembersAdapter? = null

    //за основу взята архитектура MVP, презентер содержит логику взаимодействия с ресурсами
    override val presenter = MainPresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        mAdapter = MembersAdapter()
        mAdapter!!.setOnClickListener({
            var intent = Intent(this@MainActivity,MemberDetailsActivity::class.java)
            intent.putExtra("member_id", it)
            startActivity(intent)

        })
        ac_main_team_rv.adapter = mAdapter
        ac_main_team_rv.layoutManager = LinearLayoutManager(this)

        //скрываем кнопку чтобы не мешала просматривать нижний элементЬ
        ac_main_team_rv.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                //многократный вызов - не плохо, внутри системных методов скрыть/показать стоит проверка
                Log.d("#MY ", "shown = ${fab.isShown} dy = $dy")
                if (dy > 8 && fab.isShown) { // если досточно быстрое изменение(8 px), скрываем либо показываем кнопку
                    fab.hide()
                }
                if (dy < -8 && !fab.isShown) {
                    fab.show()
                }
            }
        })

        fab.setOnClickListener { _ ->
            //запускаем экран создания участника
            startActivity(Intent(this, MemberCreateActivity::class.java))
        }
    }

    override fun onStart() {
        super.onStart()
        //чтобы успели записаться при создании приложения
        Handler().postDelayed({
            presenter.loadMembers()
        }, 100)

    }

    override fun showMembers(members: List<Member>) {
        mAdapter!!.swap(members)
    }
}
