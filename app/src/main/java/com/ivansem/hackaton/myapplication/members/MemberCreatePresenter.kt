package com.ivansem.hackaton.myapplication.members
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import com.ivansem.hackaton.myapplication.MyApp
import com.ivansem.hackaton.myapplication.R
import com.ivansem.hackaton.myapplication.base.BasePresenter
import com.ivansem.hackaton.myapplication.db.DbService
import com.ivansem.hackaton.myapplication.utils.FileUtils
import com.ivansem.hackaton.myapplication.main.Member
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by root on 06.04.18.
 */
class MemberCreatePresenter(private val view: IMemberCreateView): BasePresenter() {
    var photo: Bitmap? = null


    fun createMember(member: Member) {
        DbService.getInstance().saveMember(member)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.memberCreated()
                }, { error ->
                    view.memberNotCreated()
                    Log.d(TAG, error.message, error)
                }).connect()
    }

    companion object {
        val TAG = "#MY " + MemberCreatePresenter::class.java.simpleName
    }

    fun getPhotoUrl(): String {
        if (photo != null)
            return FileUtils.saveMemberImg(photo!!)
        else
            return FileUtils.saveMemberImg(BitmapFactory.decodeResource(MyApp.instance.resources, R.drawable.photo_placeholder))
    }
}