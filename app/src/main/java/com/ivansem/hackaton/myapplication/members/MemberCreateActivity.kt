package com.ivansem.hackaton.myapplication.members

import android.app.Activity
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Log
import android.widget.EditText
import android.widget.Toast
import com.ivansem.hackaton.myapplication.R
import com.ivansem.hackaton.myapplication.loadRoundedImage
import com.ivansem.hackaton.myapplication.main.Member
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import kotlinx.android.synthetic.main.activity_member_create.*
import ru.terrakok.gitlabclient.ui.global.BaseActivity
import java.io.InputStream


class MemberCreateActivity : BaseActivity(), IMemberCreateView {
    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode == Activity.RESULT_OK) {
                val imageStream: InputStream? = contentResolver.openInputStream(result.uri)
                presenter.photo = BitmapFactory.decodeStream(imageStream)
                new_photo_iv.loadRoundedImage(presenter.photo!!)
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                val error = result.error
                Toast.makeText(this, error.message, Toast.LENGTH_LONG).show()
            }
        }
    }

    override val presenter = MemberCreatePresenter(this)

    override fun memberCreated() {
        Toast.makeText(this, "Участник добавлен!", Toast.LENGTH_SHORT).show()
        finish()
    }

    override fun memberNotCreated() {
        Toast.makeText(this, "Участник не был добавлен", Toast.LENGTH_LONG).show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_member_create)
        setSupportActionBar(ac_member_create_toolbar)
        ac_member_create_toolbar.setNavigationOnClickListener({
            finish()
        })
        new_photo_iv.setOnClickListener({
            //            presenter.createNewPhoto()
            CropImage.activity()
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .start(this);
        })
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        create_member_btn.setOnClickListener {
            Log.d("#MY ", "clicked")
            if (
                listOf<EditText>(
                        name_et,
                        age_et,
                        group_et,
                        info_et
                ).all { !it.text.isEmpty() }
            ) {
                presenter.createMember(
                        Member(
                                name_et.text.toString(),
                                age_et.text.toString().toInt(),
                                group_et.text.toString(),
                                info_et.text.toString(),
                                presenter.getPhotoUrl()
                        )
                )
            } else {
                if(name_et.text.isEmpty()){
                    name_et.error = "Не заполнено имя"
                }
                if(age_et.text.isEmpty()){
                    age_et.error = "Не заполнен возраст"
                }
                if(group_et.text.isEmpty()){
                    group_et.error = "Не заполнена группа"
                }
                if(info_et.text.isEmpty()){
                    info_et.error = "Не заполнена дополнительная информация"
                }
            }

        }
    }
}
