package com.ivansem.hackaton.myapplication.details

import com.ivansem.hackaton.myapplication.main.Member

interface IMemberDetailsView {
    fun showMember(member: Member)
}
