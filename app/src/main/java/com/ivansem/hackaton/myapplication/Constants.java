package com.ivansem.hackaton.myapplication;

import org.jetbrains.annotations.NotNull;

public class Constants {
    public static final int SELECT_PHOTO = 1234;
    public static final int TAKE_PHOTO = 5434;
    public static final int REQUEST_STORAGE_PERMISSION = 6542;
    public static final int REQUEST_CROP = 7657;
}
