package com.ivansem.hackaton.myapplication

import android.app.Application
import android.graphics.BitmapFactory
import android.util.Log
import com.ivansem.hackaton.myapplication.db.DbService
import com.ivansem.hackaton.myapplication.main.Member
import com.ivansem.hackaton.myapplication.utils.FileUtils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.realm.Realm

/**
 * Created by root on 05.04.18.
 */
class MyApp : Application() {
    override fun onCreate() {
        super.onCreate()
        instance = this
        Log.d(TAG, "my app on create")
        Realm.init(this) //инициализация БД
        testInit()
    }

    private fun testInit() {
    //добавляем информацию о нашей команде, если БД пустая
    if (DbService.getInstance().getMemberCount() == 0) {
            val alexeyImg = FileUtils.saveMemberImg(BitmapFactory.decodeResource(resources, R.drawable.leha))
            val ivanImg = FileUtils.saveMemberImg(BitmapFactory.decodeResource(resources, R.drawable.ivan))
            val maximImg = FileUtils.saveMemberImg(BitmapFactory.decodeResource(resources, R.drawable.maxim))
            DbService.getInstance().erase()
            DbService.getInstance()
                    .saveMember(Member("Семенченко Иван", 22, "ИУ5-23М", "Учусь в магистратуре МГТУ им. Н.Э. Баумана. Работаю android-разработчиком. В свободное от работы и учебы время... такого давно не было. Нравится участвовать в разного рода хакатонах и других мероприятиях. Они дают много опыта, к тому же, это интересно.", ivanImg))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe()
            DbService.getInstance()
                    .saveMember(Member("Уймин Максим", 18, "ИУ6-23Б", "Студент 1-ого курса МГТУ им. Н.Э. Баумана. Программирую на языках Delphi Pascal, C++, Java. В 2016 окончил IT школу Samsung по Android разработке", maximImg))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe()
            DbService.getInstance()
                    .saveMember(Member("Орехов Алексей", 22, "ИУ5-23М", "Студент первого курса магистратуры МГТУ им. Н.Э. Баумана. В свободное от работы и развлечений прихожу в универ. Люблю слушать музыку, смотреть фильмы и аниме, а еще люблю котиков, есть и спать!", alexeyImg))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe()
        }
    }

    companion object {
        val TAG = "#MY " + MyApp::class.java.simpleName
        lateinit var instance: MyApp
    }
}