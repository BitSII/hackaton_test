package com.ivansem.hackaton.myapplication.base

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable


open class BasePresenter() {

    private val compositeDisposable = CompositeDisposable()

    fun onDestroy() {
        compositeDisposable.clear()
    }

    protected fun Disposable.connect() {
        compositeDisposable.add(this)
    }
}