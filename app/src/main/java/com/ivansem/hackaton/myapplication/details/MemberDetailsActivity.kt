package com.ivansem.hackaton.myapplication.details

import android.os.Bundle
import com.ivansem.hackaton.myapplication.R
import com.ivansem.hackaton.myapplication.loadRoundedImage
import com.ivansem.hackaton.myapplication.main.Member
import com.ivansem.hackaton.myapplication.utils.FileUtils
import kotlinx.android.synthetic.main.activity_details.*
import ru.terrakok.gitlabclient.ui.global.BaseActivity

class MemberDetailsActivity : BaseActivity(), IMemberDetailsView {
    override fun showMember(member: Member) {
        ac_member_details_toolbar.setTitle(member.name)

        val memberImg = FileUtils.getMemberImg(member.imgName)
        photo_iv.loadRoundedImage(memberImg.absolutePath, this@MemberDetailsActivity)
        name_tv.text = member.name
        age_tv.text = "Возраст: " + member.age.toString()
        info_tv.text = member.info
        group_tv.text = "Группа: " + member.group
    }

    var memberId: Long = -1
    override val presenter = MemberDetailsPresenter(this)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        setSupportActionBar(ac_member_details_toolbar)
        ac_member_details_toolbar.setNavigationOnClickListener({
            finish()
        })
        memberId = intent.getLongExtra("member_id", -1)
        presenter.loadMember(memberId)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

}
